import React from 'react'
import ReactDOM from 'react-dom'
import dotenv from 'dotenv'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import { reducer as formReducer } from 'redux-form'

import './index.css'
import App from './App'
import cartReducer from './store/reducers/cart'
import libraryReducer from './store/reducers/library'

const composeEnhancers = process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose

const rootReducer = combineReducers({
  library: libraryReducer,
  cart: cartReducer,
  form: formReducer
})

const store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(thunk)
))

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
)

dotenv.config()
ReactDOM.render(app, document.getElementById('root'))
