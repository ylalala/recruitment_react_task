import reducer from './library'
import * as actionTypes from '../actions/actionTypes'

describe('library reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      loading: false,
      error: null,
      books: [],
      total: NaN,
      pageNr: NaN
    })
  })

  it('should store books, upon books loaded', () => {
    expect(reducer({
      loading: false,
      error: null,
      books: [],
      total: NaN,
      pageNr: NaN
    }, {
      type: actionTypes.LOAD_BOOKS_SUCCESS,
      books: [{ id: 'someBookId' }],
      total: 1,
      pageNr: 1
    })).toEqual({
      loading: false,
      error: null,
      books: [{ id: 'someBookId' }],
      total: 1,
      pageNr: 1
    })
  })
})
