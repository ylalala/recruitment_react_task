import * as actionTypes from '../actions/actionTypes'
import { updateObject, getBookIndex } from '../../shared/utility'

const initialState = {
  booksInCart: [],
  quantity: 0,
  loading: false,
  error: null,
  finished: false
}

const addToCart = (state, action) => {
  const bookIndex = getBookIndex(state.booksInCart, action.book.id)
  let updatedBooksInCart
  if (~bookIndex) {
    updatedBooksInCart = state.booksInCart.map((book, index) => {
      if (index !== bookIndex) {
        return book
      }
      return {
        ...book,
        quantity: book.quantity + 1
      }
    })
  } else {
    updatedBooksInCart = [...state.booksInCart, { ...action.book, quantity: 1 }]
  }
  const updatedState = {
    quantity: state.quantity + 1,
    booksInCart: updatedBooksInCart
  }
  return updateObject(state, updatedState)
}
const initOrder = (state) => {
  return updateObject(state, {
    error: null,
    finished: false
  })
}
const orderBooksStart = (state) => {
  return updateObject(state, {
    loading: true,
    error: null
  })
}
const orderBooksSuccess = (state) => {
  return updateObject(state, {
    loading: false,
    error: false,
    finished: true,
    booksInCart: [],
    quantity: 0
  })
}
const orderBooksFail = (state) => {
  return updateObject(state, {
    loading: false,
    error: true
  })
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_TO_CART: return addToCart(state, action)
    case actionTypes.INIT_ORDER: return initOrder(state)
    case actionTypes.ORDER_BOOKS_START: return orderBooksStart(state)
    case actionTypes.ORDER_BOOKS_SUCCESS: return orderBooksSuccess(state)
    case actionTypes.ORDER_BOOKS_FAIL: return orderBooksFail(state)
    default: return state
  }
}

export default reducer
