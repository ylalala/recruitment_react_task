import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../../shared/utility'

const initialState = {
  loading: false,
  error: null,
  books: [],
  total: NaN,
  pageNr: NaN
}

const loadBooksStart = (state, action) => {
  return updateObject(state, {
    pageNr: +action.pageNr,
    loading: true,
    error: null
  })
}

const loadBooksSuccess = (state, action) => {
  return updateObject(state, {
    loading: false,
    error: null,
    books: action.books,
    total: +action.total,
    pageNr: +action.pageNr
  })
}

const loadBooksFail = (state, action) => {
  return updateObject(state, {
    loading: true,
    error: true,
    books: [],
    total: NaN,
    pageNr: NaN
  })
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOAD_BOOKS_START: return loadBooksStart(state, action)
    case actionTypes.LOAD_BOOKS_SUCCESS: return loadBooksSuccess(state, action)
    case actionTypes.LOAD_BOOKS_FAIL: return loadBooksFail(state, action)
    default: return state
  }
}

export default reducer
