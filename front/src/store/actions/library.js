import axios from '../../axios'
import * as actionTypes from './actionTypes'

export const loadBooks = ({ pageNr = 1, title = '', author = '' }) => {
  return dispatch => {
    dispatch(loadBooksInit(pageNr))
    axios.get('/book', {
      params: {
        page: pageNr,
        'search[title]': title,
        'search[author]': author
      }
    })
      .then((response) => {
        dispatch(loadBooksSuccess(response.data))
      }, (error) => {
        dispatch(loadBooksFail(error))
      })
  }
}
const loadBooksSuccess = (data) => {
  return {
    type: actionTypes.LOAD_BOOKS_SUCCESS,
    books: data.data,
    total: data.metadata.total_records,
    pageNr: data.metadata.page
  }
}
const loadBooksInit = (pageNr) => {
  return {
    type: actionTypes.LOAD_BOOKS_START,
    pageNr: pageNr
  }
}
const loadBooksFail = () => {
  return {
    type: actionTypes.LOAD_BOOKS_FAIL
  }
}
