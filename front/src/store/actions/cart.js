import * as actionTypes from './actionTypes'
import axios from '../../axios'

export const addToCart = (book) => {
  return {
    type: actionTypes.ADD_TO_CART,
    book: book
  }
}

export const initOrder = () => {
  return {
    type: actionTypes.INIT_ORDER
  }
}

export const orderBooks = (form) => {
  return dispatch => {
    dispatch(orderBooksStart())
    axios.post('/order', form)
      .then(() => {
        dispatch(orderBooksSuccess())
      }, (error) => {
        dispatch(orderBooksFail(error))
      })
  }
}

const orderBooksSuccess = () => {
  return {
    type: actionTypes.ORDER_BOOKS_SUCCESS
  }
}
const orderBooksStart = () => {
  return {
    type: actionTypes.ORDER_BOOKS_START
  }
}
const orderBooksFail = () => {
  return {
    type: actionTypes.LOAD_BOOKS_FAIL
  }
}
