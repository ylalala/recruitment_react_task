import React, { Fragment } from 'react'
import { connect } from 'react-redux'

import Layout from '../../components/UI/Layout/Layout'
import ProgressBar from '../../components/UI/ProgressBar/ProgressBar'
import CartList from '../../components/CartList/CartList'
import CartSummary from '../../components/CartSummary/CartSummary'

const Cart = ({ booksInCart }) => (
  <Layout
    header={booksInCart.length ? <ProgressBar step='1' /> : 'Masz pusty koszyk'}>
    <hr />
    {booksInCart.length > 0 &&
      <Fragment>
        <CartList booksInCart={booksInCart} />
        <CartSummary booksInCart={booksInCart} />
      </Fragment>
    }
  </Layout>
)

const mapStateToProps = state => (
  {
    booksInCart: state.cart.booksInCart
  }
)

export default connect(mapStateToProps)(Cart)
