import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import * as actions from '../../store/actions'

import * as paths from '../../shared/paths'
import Layout from '../../components/UI/Layout/Layout'
import ProgressBar from '../../components/UI/ProgressBar/ProgressBar'
import OrderForm from '../../components/OrderForm/OrderForm'
import Spinner from '../../components/UI/Spinner/Spinner'
import Thanks from '../../components/Thanks/Thanks'

export class Order extends Component {
  constructor (props) {
    super()

    this.submitHandler = this.submitHandler.bind(this)

    props.init()
  }

  submitHandler (values) {
    const { street, buildingNumber, ...restValues } = values
    const form = { ...restValues,
      address: `${street} ${buildingNumber}`,
      order: this.props.booksInCart.map(book => (
        {
          id: book.id,
          quantity: book.quantity
        }
      ))
    }
    this.props.orderBooks(form)
  }

  render () {
    return (
      <Layout
        header={this.props.finished
          ? <ProgressBar step='3' />
          : this.props.loading
            ? <Spinner />
            : this.props.booksInCart.length
              ? <ProgressBar step='2' />
              : <Redirect to={paths.MAIN} />}>
        <hr />
        {this.props.finished ? <Thanks /> : <OrderForm onSubmit={this.submitHandler} /> }
      </Layout>
    )
  }
}

const mapStateToProps = state => {
  return {
    booksInCart: state.cart.booksInCart,
    quantity: state.cart.quantity,
    finished: state.cart.finished,
    error: state.cart.error,
    loading: state.cart.loading
  }
}

const mapDispatchToProps = dispatch => {
  return {
    orderBooks: (form) => dispatch(actions.orderBooks(form)),
    init: () => dispatch(actions.initOrder())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Order)
