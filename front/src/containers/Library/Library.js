import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../store/actions'

import * as paths from '../../shared/paths'
import Pagination from '../../components/UI/Pagination/Pagination'
import { getBookIndex } from '../../shared/utility'
import Layout from '../../components/UI/Layout/Layout'
import Input from '../../components/UI/Input/Input'
import Spinner from '../../components/UI/Spinner/Spinner'
import Gallery from '../../components/Gallery/Gallery'

export class Library extends Component {
  constructor () {
    super()
    this.goToPageHandler = this.goToPageHandler.bind(this)
    this.addToCartHandler = this.addToCartHandler.bind(this)
    this.singleOrderHandler = this.singleOrderHandler.bind(this)
    this.controls = [
      {
        key: 'titleFieldControl',
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Wpisz nazwę książki'
        },
        changed: this.inputChangedHandler.bind(this, 'titleFieldControl')
      }, {
        key: 'authorFieldControl',
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Wpisz autora'
        },
        changed: this.inputChangedHandler.bind(this, 'authorFieldControl')
      }
    ]
    this.timeout = null

    this.state = {
      titleFieldControl: '',
      authorFieldControl: ''
    }
  }

  addToCartHandler (event) {
    const { books, addToCart } = this.props
    const bookIndex = getBookIndex(books, +event.target.name)
    addToCart(books[bookIndex])
  }

  singleOrderHandler () {
    this.props.history.push(paths.CART)
  }

  componentDidMount () {
    this.props.loadBooks()
  }

  setTimeoutLoading () {
    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => {
      this.props.loadBooks({
        title: this.state.titleFieldControl,
        author: this.state.authorFieldControl
      })
    }, 200)
  }

  inputChangedHandler (fieldControlName, event) {
    this.setState({ [fieldControlName]: event.target.value })
    this.setTimeoutLoading()
  }

  goToPageHandler (pageNr) {
    this.props.loadBooks({
      title: this.state.titleFieldControl,
      author: this.state.authorFieldControl,
      pageNr: pageNr
    })
  }

  render () {
    return (
      <Layout
        header={
          <Fragment>
            {this.controls.map(control => <Input {...control} />)}
          </Fragment>
        }>
        <hr />
        {this.props.books && this.props.books.length > 0
          ? <Pagination
            total={this.props.total}
            pageNr={this.props.pageNr}
            onGoToPage={this.goToPageHandler}
          />
          : this.props.loading
            ? <Spinner /> : 'Nie znaleziono książki o podanych parametrach.'}
        <hr />
        <Gallery
          books={this.props.books}
          onAddToCart={this.addToCartHandler}
          onSingleOrder={this.singleOrderHandler}
        />
      </Layout>
    )
  }
}

const mapStateToProps = state => {
  return {
    books: state.library.books,
    total: state.library.total,
    pageNr: state.library.pageNr,
    loading: state.library.loading,
    error: state.library.error,
    quantity: state.cart.quantity
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loadBooks: (config = {}) => dispatch(actions.loadBooks(config)),
    addToCart: (book) => dispatch(actions.addToCart(book))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Library)
