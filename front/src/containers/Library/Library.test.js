import React from 'react'
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import { Library } from './Library'
import Pagination from '../../components/UI/Pagination/Pagination'

configure({ adapter: new Adapter() })

describe('<Library />', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(<Library onLoadBooks={() => {}} />)
  })

  it('should render <Pagination /> when receiving books', () => {
    wrapper.setProps({ books: [{ id: 'somebook' }] })
    expect(wrapper.find(Pagination)).toHaveLength(1)
  })
})
