import React from 'react'
import { Field, reduxForm } from 'redux-form'
import styled from 'styled-components'
import { withRouter } from 'react-router-dom'

import { required, number, email, minLength9, postCode } from '../../shared/utility'
import Input from '../UI/Input/FormInput'
import Button from '../UI/Button/Button'

const StyledForm = styled.form`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  flex-wrap: wrap;
  > div {
    font-size: 0.9rem;
    color: #404040;
    padding: 1rem;
    margin: 0.5rem;
    border: 1px solid #eee;
    box-sizing: border-box;
    background-color: #fafafa;
    box-shadow: 0.5rem 0.5rem 1.5rem 0 #ddd;
    flex-grow:1;
    &:nth-child(3) {
      display: flex;
      justify-content: space-between;
      align-items: center;
      width:100%;
    }
  }
`

let OrderForm = props => {
  return <StyledForm onSubmit={props.handleSubmit}>
    <div>
      <Field
        name='first_name'
        component={Input}
        validate={[ required ]}
        label='Imię'
      />
      <Field
        name='last_name'
        component={Input}
        validate={[ required ]}
        label='Nazwisko'
      />
      <Field
        name='phone_number'
        component={Input}
        validate={[ required, number, minLength9 ]}
        label='Numer kontaktowy'
      />
      <Field
        name='email'
        component={Input}
        validate={[ required, email ]}
        label='Adres e-mail'
      />
    </div>
    <div>
      <Field
        name='street'
        component={Input}
        validate={[ required ]}
        label='Ulica'
      />
      <Field
        name='buildingNumber'
        component={Input}
        validate={[ required ]}
        label='Numer budynku'
      />
      <Field
        name='city'
        component={Input}
        validate={[ required ]}
        label='Miejscowość'
      />
      <Field
        name='zip_code'
        component={Input}
        validate={[ required, postCode ]}
        label='Kod pocztowy'
      />
    </div>
    <div>
      <Button clicked={props.history.goBack} >Cofnij</Button>
      <Button type='submit'>Zamów i zapłać</Button>
    </div>
  </StyledForm>
}

OrderForm = reduxForm({
  form: 'order'
})(OrderForm)

export default withRouter(OrderForm)
