import React from 'react'
import styled from 'styled-components'

const ProgressBarWrapper = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  margin: 0 1rem;
  padding-bottom: 1.5rem;
  > .line {
    flex-grow: 1;
    height: 8px;
    background-color: #fafafa;
    border-top: 1px solid #eee;
    border-bottom: 1px solid #eee;
  }
  > .step {
    width: 3rem;
    height: 3rem;
    border: 1px solid #eee;
    background-color: #fafafa;
    border-radius: 50%;
    position: relative;
    > span {
      position: absolute;
      left: 50%;
      transform: translate(-50%);
      bottom: -1.5rem;
    }
    &:nth-child(1){
      &::after {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        content: '1';
      }
    }
    &:nth-child(3) {
      &::after{
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        content: '2';
      }
    }
    &:nth-child(5) {
      &::after{
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        content: '3';
      }
    }
    &.active {
      background-color: #e5dede;
    }
    &.done {
      background-color: #bfbfbf;
      border-color:#bfbfbf;
      & + .line {
        background-color: #bfbfbf;
      }
    }
  }
`
const ProgressBar = (props) => {
  const config = {}
  switch (+props.step) {
    case 1:
      config.step1 = 'step active'
      config.step2 = config.step3 = 'step'
      break
    case 2:
      config.step1 = 'step done'
      config.step2 = 'step active'
      config.step3 = 'step'
      break
    case 3:
    default:
      config.step1 = config.step2 = 'step done'
      config.step3 = 'step active'
      break
  }
  return (
    <ProgressBarWrapper>
      <div
        className={config.step1}>
        <span>Koszyk</span>
      </div>
      <div className='line' />
      <div
        className={config.step2}>
        <span>Dane</span>
      </div>
      <div className='line' />
      <div
        className={config.step3}>
        <span>Gotowe!</span>
      </div>
    </ProgressBarWrapper>
  )
}
export default ProgressBar
