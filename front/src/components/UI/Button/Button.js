import React from 'react'
import styled from 'styled-components'

const StyledButton = styled.button`
  display: block;
  margin: 0.25rem;
  background-color: #eee;
  border: 1px solid #ddd;
  outline: none;
  padding: 0.5rem;
  font-size: 0.8rem;
  font-weight: bold;
  color: #484848;
  cursor: pointer;
  &:hover,
  &:focus {
    background-color: #e5dede;
  }
  &:active {
    background-color: #999;
  }
`
const Button = (props) => (
  <StyledButton
    type={props.type ? props.type : 'button'}
    name={props.name}
    disabled={props.disabled}
    onClick={props.clicked}>{props.children}</StyledButton>
)

export default Button
