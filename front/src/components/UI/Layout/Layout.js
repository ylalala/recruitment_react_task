import React from 'react'
import styled from 'styled-components'

import Header from '../Header/Header'
import Footer from '../Footer/Footer'

const StyledWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  box-sizing: border-box;
  font-size: 20px;
  background-color: whitesmoke;
  padding-bottom: 30px;
  min-height: 100%;
`

const Layout = (props) => (
  <StyledWrapper>
    <Header>
      {props.header}
    </Header>
    <main>{props.children}</main>
    <Footer />
  </StyledWrapper>
)

export default Layout
