import React from 'react'
import styled from 'styled-components'

const StyledFooter = styled.footer`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  height: 1.5rem;
`
const Footer = ({ children }) => (
  <StyledFooter>{children}</StyledFooter>
)

export default Footer
