import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import * as paths from '../../../shared/paths'
import CartSymbol from '../../CartSymbol/CartSymbol'

const StyledHeader = styled.header`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0.5rem;
  flex-wrap: wrap;
  position: relative;
  color: #404040;
  > h1 {
    margin-top: 0;
    width:100%;
    cursor: pointer;
    > a {
      color: #404040;
    }
  }
`

const Header = (props) => (
  <StyledHeader>
    <h1><Link to={paths.MAIN}>Księgarnia</Link></h1>
    {props.children}
    <CartSymbol />
  </StyledHeader>
)

export default Header
