import React from 'react'
import styled from 'styled-components'

const StyledInput = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  > label {
    font-size: 0.6rem;
    margin-right: 0.5rem;
    margin-bottom: 1rem;
  }
  > div {
    flex-grow: 1;
    display: flex;
    justify-content: flex-start;
    flex-direction: column;
    > input {
      width: 100%;
      box-sizing: border-box;
    }
    > span {
      min-height: 1rem;
      font-size: 0.5rem;
      color: #000;
      font-weight: bold;
    }
  }
`
const RenderField = ({ input, label, meta: { touched, error, warning } }) => (
  <StyledInput>
    <label>{label}</label>
    <div>
      <input {...input} placeholder={label} />
      <span>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</span>
    </div>
  </StyledInput>
)

export default RenderField
