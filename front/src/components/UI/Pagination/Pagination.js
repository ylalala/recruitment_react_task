import React from 'react'
import styled from 'styled-components'

import PageLink from './PageLink/PageLink'
import Controllers from './Controllers/Controllers'

const StyledPagination = styled.ul`
  width: 100%;
  font-size: 0.8rem;
  box-sizing: border-box;
`
const Pagination = (props) => {
  const controllersLength = Math.ceil(props.total / 10)
  return (
    <StyledPagination>
      <PageLink
        goTo={Math.max(props.pageNr - 1, 1)}
        disabled={props.pageNr === 1}
        clicked={props.onGoToPage}>Poprzednia</PageLink>
      <Controllers
        controllersLength={controllersLength}
        total={props.total}
        pageNr={props.pageNr}
        onGoToPage={props.onGoToPage} />
      <PageLink
        goTo={Math.min(props.pageNr + 1, controllersLength)}
        disabled={props.pageNr === controllersLength}
        clicked={props.onGoToPage}>Następna</PageLink>
    </StyledPagination>
  )
}

export default Pagination
