import React from 'react'

import PageLink from '../PageLink/PageLink'

const Controllers = ({ total, pageNr, onGoToPage, controllersLength }) => (
  [...new Array(controllersLength)].map((_, i) => {
    return (
      <PageLink
        key={i}
        goTo={i + 1}
        active={pageNr === i + 1}
        disabled={pageNr === i + 1}
        clicked={onGoToPage}
      >{i + 1}</PageLink>
    )
  })
)

export default Controllers
