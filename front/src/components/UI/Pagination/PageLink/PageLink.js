import React from 'react'
import styled from 'styled-components'

const StyledPageLink = styled.li`
  display: inline-block;
  margin: 0.5rem;
  cursor: pointer;
  color: #404040;
  ${props => props.disabled ? `
    color:#a6a6a6;
    cursor: default` : null}
  ${props => props.active ? `
    font-weight: bold;
    cursor: default;
    color: #404040;` : null}
`
const PageLink = ({ active, disabled, clicked, goTo, children }) => (
  <StyledPageLink
    active={active}
    disabled={disabled}
    onClick={disabled
      ? null
      : () => { clicked(goTo) }}>{children}</StyledPageLink>
)

export default PageLink
