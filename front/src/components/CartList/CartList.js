import React from 'react'
import styled from 'styled-components'

import CartElem from './CartElem/CartElem'

const StyledCartList = styled.div`
    font-size: 0.9rem;
    color: #404040;
    padding: 1rem;
    margin: 0.5rem;
    border: 1px solid #eee;
    box-sizing: border-box;
    background-color: #fafafa;
    box-shadow: 0.5rem 0.5rem 1.5rem 0 #ddd;
    > .headerRow {
      display: flex;
      justify-content: space-between;
      align-items: center;
      margin-bottom: 1rem;
      > div:nth-child(1){
        width: 50%;
      }
    }
`
const CartList = ({ booksInCart }) => (
  <StyledCartList>
    <div className='headerRow'>
      <div>Produkt</div>
      <div>Ilość</div>
      <div>Cena</div>
    </div>
    {booksInCart.map(book => {
      return (
        <CartElem
          key={book.id}
          author={book.author}
          title={book.title}
          price={book.price}
          currency={book.currency}
          coverUrl={book.cover_url}
          quantity={book.quantity}
        />
      )
    })}
  </StyledCartList>
)

export default CartList
