import React from 'react'
import styled from 'styled-components'

import { numberToPrice } from '../../../shared/utility'

const StyledCartElem = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 1rem;
  > .details {
    display: flex;
    align-items: center;
    width: 50%;
    > div {
      display: flex;
      flex-direction: column;
      margin-left: 0.5rem;
      > div {
        line-height: 1.3;
        :nth-child(1){
          font-weight: bold;
          margin-bottom: 0.25rem;
        }
        :nth-child(2){
          font-size: 0.75rem;
        }
      }
    }
    > img {
      width: 4rem;
      box-shadow: 0.2rem 0.2rem 0.6rem 0 #ccc;
    }
  }
`
const cartElem = (props) => {
  return (
    <StyledCartElem>
      <div className='details'>
        <img src={props.coverUrl} alt='' />
        <div>
          <div>{props.title}</div>
          <div>{props.author}</div>
        </div>
      </div>
      <div>
        {props.quantity}
      </div>
      <div>
        {numberToPrice(props.price * props.quantity, props.currency)}
      </div>
    </StyledCartElem>
  )
}

export default cartElem
