import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import * as paths from '../../shared/paths'
import Button from '../UI/Button/Button'

const StyledThanks = styled.div`
  display: flex;
  font-size: 1.5rem;
  color: #404040;
  padding: 1rem;
  margin: 0.5rem;
  border: 1px solid #eee;
  box-sizing: border-box;
  background-color: #fafafa;
  box-shadow: 0.5rem 0.5rem 1.5rem 0 #ddd;
  justify-content: space-between;
  align-items: center;
  > div:nth-child(2) {
    font-size: 0.9rem;
  }
`
const Thanks = () => (
  <StyledThanks>
    <div>Gratulacje! Zamówienie zostało złożone</div>
    <div>
      <Link to={paths.MAIN}>
        <Button>Wróć do księgarni</Button>
      </Link>
    </div>
  </StyledThanks>
)

export default Thanks
