import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import * as paths from '../../shared/paths'
import { numberToPrice } from '../../shared/utility'
import Button from '../UI/Button/Button'

const StyledCartSummary = styled.div`
    font-size: 0.9rem;
    color: #404040;
    padding: 1rem;
    margin: 0.5rem;
    border: 1px solid #eee;
    box-sizing: border-box;
    background-color: #fafafa;
    box-shadow: 0.5rem 0.5rem 1.5rem 0 #ddd;
    > .headerRow {
      display: flex;
      justify-content: space-between;
      align-items: center;
      margin-bottom: 1rem;
      > div:nth-child(1){
        width: 50%;
      }
    }
    > .controllers {
      display: flex;
      justify-content: space-between;
      align-items: center;
    }
  
`
const CartSummary = (props) => {
  const total = {
    quantity: 0,
    cost: {}
  }

  props.booksInCart.reduce((previousTotal, book) => {
    previousTotal.quantity += book.quantity
    previousTotal.cost[book.currency]
      ? previousTotal.cost[book.currency] += book.price * book.quantity
      : previousTotal.cost[book.currency] = book.price * book.quantity
    return previousTotal
  }, total)

  return (
    <StyledCartSummary>
      <div className='headerRow'>
        <div>Łącznie</div>
        <div>{total.quantity}</div>
        <div>{Object.keys(total.cost).map((currency) => (
          <div key={currency}>{numberToPrice(total.cost[currency], currency)}</div>
        ))}
        </div>
      </div>
      <div className='controllers'>
        <Link to={paths.MAIN}><Button>Wróć do księgarni</Button></Link>
        <Link to={paths.ORDER}><Button>Dalej</Button></Link>
      </div>
    </StyledCartSummary>
  )
}

export default CartSummary
