import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { connect } from 'react-redux'

import * as paths from '../../shared/paths'
import cartImg from '../../assets/cart.svg'

const CartContainer = styled.div`
  display: block;
  position: absolute;
  right: 10px;
  top: 0;
  transition: transform 0.3s;
  :hover{
    transform:scale(1.3)
  }
  a {
    display: block;
    line-height: 0;
    color: #404040;
    font-size: 0.8rem;
    ::after{
      display: block;
      position: absolute;
      content:  ${props => props.quantity ? '"' + props.quantity + '"' : '"0"'};
      transform: translate(-50%,-50%);
      top: 50%;
      left: 50%;
    }
  }
`
const CartSymbol = (props) => (
  <CartContainer quantity={props.quantity}>
    <Link to={paths.CART}>
      <img src={cartImg} alt='Cart' />
    </Link>
  </CartContainer>
)

const mapStateToProps = state => (
  {
    quantity: state.cart.quantity
  }
)

export default connect(mapStateToProps)(CartSymbol)
