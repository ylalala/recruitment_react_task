import React from 'react'
import styled from 'styled-components'
import Button from '../../UI/Button/Button'
import { numberToPrice } from '../../../shared/utility'

const StyledWrapper = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  font-size: 0.5rem;
  padding: 1rem;
  margin: 0.5rem;
  border: 1px solid #eee;
  box-sizing: border-box;
  background-color: #fafafa;
  color: #404040;
  box-shadow: 0.5rem 0.5rem 1.5rem 0 #ddd;
  img {
    width: 7rem;
    box-shadow: 0.2rem 0.2rem 0.6rem 0 #ccc;
  }
  > div {
    display: flex;
  }
  @media (min-width: 320px){
    width: 100%;
  }
  @media (min-width: 768px){
    width: calc(50% - 2rem);
  }
  @media (min-width: 1024px){
    width: calc(33.33% - 1rem);
  }
  @media (min-width: 1440px){
    width: calc(25% - 1rem );
  }
`
const StyledDetailsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  font-size: 0.8rem;
  padding-left: 0.5rem;
  flex-grow: 1;
  > div{
    display: flex;
    flex-direction: column;
  }
`
const Book = (props) => (
  <StyledWrapper>
    <h2>{props.title}</h2>
    <h3>{props.author}</h3>
    <div>
      <div><img src={props.cover_url} alt='' /></div>
      <StyledDetailsWrapper>
        <div>
          <div>Liczba stron: {props.pages}</div>
          <div>Cena: {numberToPrice(props.price, props.currency)}</div>
        </div>
        <div>
          <Button name={props.id} clicked={props.onOrder}>Zamów</Button>
          <Button name={props.id} clicked={props.onAddToCart}>Dodaj do koszyka</Button>
        </div>
      </StyledDetailsWrapper>
    </div>
  </StyledWrapper>
)

export default Book
