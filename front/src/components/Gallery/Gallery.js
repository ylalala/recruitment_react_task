import React from 'react'
import styled from 'styled-components'

import Book from './Book/Book'

const StyledDiv = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;
`
const Gallery = (props) => (
  <StyledDiv>
    {props.books.map(book => {
      return (
        <Book
          {...book}
          onOrder={props.onSingleOrder}
          onAddToCart={props.onAddToCart}
          key={book.id}
        />
      )
    })}
  </StyledDiv>
)

export default Gallery
