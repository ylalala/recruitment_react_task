import React, { Component, Fragment } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'

import Library from './containers/Library/Library'
import Cart from './containers/Cart/Cart'
import Order from './containers/Order/Order'
import * as paths from './shared/paths'

class App extends Component {
  render () {
    return (
      <Fragment>
        <Switch>
          <Route path={paths.MAIN} exact component={Library} />
          <Route path={paths.CART} component={Cart} />
          <Route path={paths.ORDER} component={Order} />
          <Redirect to={paths.MAIN} />
        </Switch>
      </Fragment>
    )
  }
}

export default App
