export const updateObject = (oldObject, updatedProperties) => {
  return {
    ...oldObject,
    ...updatedProperties
  }
}

export const numberToPrice = (number, currency = 'PLN', locale = 'pl-PL') => {
  return Intl.NumberFormat.call(this, locale, {
    style: 'currency',
    currency: currency
  }).format(number / 100)
}

export const getBookIndex = (books, bookId) => {
  return books.findIndex((e) => {
    return e.id === +bookId
  })
}

export const postCode = value => value && !/\d{2}-\d{3}/.test(value)
  ? 'Zły kod pocztowy' : undefined
export const required = value => value ? undefined : 'Wartość jest wymagana'
export const maxLength = max => value =>
  value && value.replace(/\s/g, '').length > max ? `Maksymalnie ${max} znaków` : undefined
export const minLength = min => value =>
  value && value.replace(/\s/g, '').length < min ? `Minimalnie ${min} znaków` : undefined
export const maxLength15 = maxLength(15)
export const minLength9 = minLength(9)
export const number = value => value && isNaN(Number(value.replace(/\s/g, ''))) ? 'Wartość nie jest numerem' : undefined
export const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? 'Wartość nie jest adresem e-mail' : undefined
